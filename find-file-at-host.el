;;; find-file-at-host-with-ido.el --- specify a host for ido-find-file

;; Copyright (C) 2016 NGK Sternhagen

;; Author: NGK Sternhagen <sternhagen@protonmail.ch>
;; Version: 0.0.0
;; Keywords: ido tramp
;; URL: https://gitlab.com/skwuent/shell-from-here

;;; Commentary:

;; prompt the user to specify a host and change directories before executing
;; ido-find-file

(setq find-file-at-host--use-ido t);
(setq find-file-at-host--use-helm 'nil);

;; TODO prompt for name of file if neither ido nor helm are present
;; TODO account for case when both --use-ido and --use-helm are true

;;;###autoload
(defun find-file-at-host (target-host)
  "find file on TARGET-HOST via M-x find-file-at-host,
defaulting to home directory on the local host if TARGET-HOST is
the empty string."
    (interactive "sEnter host > ")
    (let ((find-dir-default "~"))
        (if (string= target-host "")
            (cd find-dir-default)
          (cd (format "/%s:/" target-host)))
      (if (equal find-file-at-host--use-ido t)
          (progn    (require 'ido)
                    (ido-find-file)))
      (if (equal find-file-at-host--use-helm t)
          (progn    (require 'helm)
                    (message "not implemented yet")))))
